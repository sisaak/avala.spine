﻿var Avala = Avala || {};
Avala.Spine = Avala.Spine || {};

Avala.Spine.Basic = (function ($, Spine) {
    

    //create model classes
    var Person = Spine.Model.sub({});
    Person.configure('Person', 'FirstName', 'LastName', 'EmailAddress', 'IsSelected');
    Person.include({
        fullName: function () {
            return this.FirstName + ' ' + this.LastName;
        }
    });

    //create controllers
    var PersonGrid = Spine.Controller.sub({
        //selector for the element
        el: $('#person-page'),
        //elements are defined in the following format: 'selector': 'name', 
        //the result of the selector is added to the this.name variable in the controller
        elements: {
            'tbody': 'body' 
        },
        //events are defined in the following format: 'event selector': 'function' 
        //when the event is raised on the selector's dom object(s), the function is called
        events: {
            'click .alert-button': 'alertSelected'
        },
        init: function () {
            if (!this.items) throw '@items required';

            this.addMany(this.items);
        },
        addMany: function (items) {
            var controller = this;
            $.each(items, function (index, item) { controller.addOne(item); });
        },
        addOne: function (item) {
            var personRow = new PersonRow({ item: item });

            this.body.append(personRow.render().el);
        },
        alertSelected: function (e) {
            //prevent the event from bubbling up
            if (e) e.preventDefault();

            //find all selected people
            var selectedPeople = [];
            $.each(Person.all(), function (index, person) {
                if (person.IsSelected) {
                    //using the Person model's fullName extension function here.
                    selectedPeople.push(person.fullName());
                }
            });

            alert('These people are selected: ' + selectedPeople.join(', '));
        }
    });

    var PersonRow = Spine.Controller.sub({
        //the default element type of a controller is a div element, you can change this using the tag variable
        tag: 'tr',
        events: {
            'click': 'toggleSelected'
        },
        //using the jquery tmpl library here
        template: function (item) {
            return $('#person-row-template').tmpl(item);
        },
        init: function () {
            if (!this.item) throw '@item required';

            //listen for the update event on this controller's item, when it is fired, call the render function to (re)render.
            //the proxy method calls the function parameter and sets the controller to 'this'
            this.item.bind('update', this.proxy(this.render));
        },
        render: function (item) {
            if (item) this.item = item;
            //set the inner html of the controller's element to the template's merged html result
            this.html(this.template(this.item));
            return this;
        },
        toggleSelected: function (e) {
            if (e) e.preventDefault();
            //reverse the IsSelected property and save
            this.item.IsSelected = !this.item.IsSelected;
            this.item.save();
        }
    });

    return {
        init: function (model, config) {
            //use the given model parameter to set up the Person model objects and then create the master Controller
            var people = [];
            $.each(model, function (index, item) {
                var person = new Person({ FirstName: item.FirstName, LastName: item.LastName, EmailAddress: item.EmailAddress, IsSelected: false });
                person.save();
                people.push(person);
            });

            var app = new PersonGrid({ items: people });
        }
    };
})(jQuery, Spine);

Avala.Spine.Stack = (function ($, Spine) {


    //create model classes
    var Person = Spine.Model.sub({});
    Person.configure('Person', 'Id', 'FirstName', 'LastName', 'EmailAddress');
    Person.include({
        fullName: function () {
            return this.FirstName + ' ' + this.LastName;
        }
    });

    //create controllers
    var PersonGrid = Spine.Controller.sub({
        el: $('#person-page'),
        elements: {
            'tbody': 'body'
        },
        init: function () {
            //the parent stack is set to the controller.stack variable
            if (!this.stack.items) throw '@items required';

            this.addMany(this.stack.items);
        },
        addMany: function (items) {
            var controller = this;
            $.each(items, function (index, item) { controller.addOne(item); });
        },
        addOne: function (item) {
            var personRow = new PersonRow({ item: item });
            this.body.append(personRow.render().el);
        },
        active: function (parms) {
            //window.location.hash = '#/grid';
            this.constructor.__super__.active.apply(this, arguments);
        }
    });

    var PersonRow = Spine.Controller.sub({
        tag: 'tr',
        events: {
            'click': 'clicked'
        },
        template: function (item) {
            return $('#person-row-template').tmpl(item);
        },
        init: function () {
            if (!this.item) throw '@item required';

            this.item.bind('update', this.proxy(this.render));
        },
        render: function (item) {
            if (item) this.item = item;
            this.html(this.template(this.item));
            return this;
        },
        clicked: function (e) {
            if (e) e.preventDefault();
            this.navigate('/edit', this.item.Id);
        }
    });
    var PersonEdit = Spine.Controller.sub({
        el: $('#person-edit-page'),
        events: {
            'click .save-button': 'save',
            'click .cancel-button': 'cancel'
        },
        elements: {
            '.first-name-input': 'firstname',
            '.last-name-input': 'lastname',
            '.email-input': 'emailaddress'
        },
        init: function () {
            
        },
        active: function (parms) {
            var controller = this;
            if (parms.id) {
                //find the person with the given id
                $.each(Person.all(), function (index, person) {
                    if (person.Id == parms.id) {
                        controller.item = person;
                    }
                });

                //set the elements to have the values of the person
                this.firstname.val(this.item.FirstName);
                this.lastname.val(this.item.LastName);
                this.emailaddress.val(this.item.EmailAddress);
            }
            //call the base controller's active function
            this.constructor.__super__.active.apply(this, arguments);
        },
        save: function (e) {
            if (e) e.preventDefault();
            //update the person model with the input values and save
            this.item.FirstName = this.firstname.val();
            this.item.LastName = this.lastname.val();
            this.item.EmailAddress = this.emailaddress.val();
            this.item.save();
            //navigate back to the grid page
            this.navigate('/grid');
            //reset the input elements
            this.firstname.val('');
            this.lastname.val('');
            this.emailaddress.val('');
        },
        cancel: function (e) {
            if (e) e.preventDefault();
            //just navigate back to the grid page
            this.navigate('/grid');
        }
    });

    var PersonApp = Spine.Stack.sub({
        el: $('#person-app'),
        controllers: {
            grid: PersonGrid,
            edit: PersonEdit
        },
        init: function() {
            if (!this.items) throw '@items required';
            if (!this.config) throw '@config required';
        },
        //register the routes that will be used for navigation and which controllers they are associated with
        //the :id portion specifies a parameter that can be sent during navigation
        routes: {
            '/grid': 'grid',
            '/edit/:id': 'edit'
        },
        //set the default controller that will be used if no valid path controller path is used 
        default: 'grid'
    });

    return {
        init: function (model, config) {
            var people = [];
            $.each(model, function (index, item) {
                var person = new Person({ Id: item.Id, FirstName: item.FirstName, LastName: item.LastName, EmailAddress: item.EmailAddress });
                person.save();
                people.push(person);
            });

            
            var app = new PersonApp({ items: people, config: config });
            Spine.Route.setup({
                redirect: function (path, options) {
                    app.grid.active();
                }
            });
        }
    };
})(jQuery, Spine);