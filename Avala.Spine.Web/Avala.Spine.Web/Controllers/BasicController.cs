﻿using Avala.Spine.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Avala.Spine.Web.Controllers
{
    public class BasicController : Controller
    {
        //
        // GET: /Basic/

        public ActionResult Index()
        {
            var people = PersonModel.GetTestPeople();
            return View(people);
        }

    }
}
