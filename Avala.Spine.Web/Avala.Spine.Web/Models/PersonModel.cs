﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Avala.Spine.Web.Models
{
    public class PersonModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }

        public static List<PersonModel> GetTestPeople()
        {
            return (new List<PersonModel>
            {
                new PersonModel { Id = 0, FirstName = "Dan", LastName = "Ramler", EmailAddress = "danr@avalamerketing.com" },
                new PersonModel { Id = 1, FirstName = "Brian", LastName = "Wagener", EmailAddress = "brian@wag.nr" },
                new PersonModel { Id = 2, FirstName = "Brian", LastName = "Behrens", EmailAddress = "brianb@avalamarketing.com" },
                new PersonModel { Id = 3, FirstName = "Jonathan", LastName = "Domian", EmailAddress = "jonathand@avalamarketing.com" },
                new PersonModel { Id = 4, FirstName = "Josh", LastName = "Tyree", EmailAddress = "josht@avalamarketing.com" },
                new PersonModel { Id = 5, FirstName = "Josh", LastName = "Riley", EmailAddress = "joshr@avalamarketing.com" },
                new PersonModel { Id = 6, FirstName = "Kevin", LastName = "Gerth", EmailAddress = "kmgerth@gmail.com" },
                new PersonModel { Id = 7, FirstName = "Dylan", LastName = "Creighton", EmailAddress = "dylanc@avalamarketing.com" }
            }).OrderBy(p => p.LastName).ThenBy(p => p.FirstName).ToList();
        }
    }
}